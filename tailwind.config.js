/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'primary': ['SphereFez', 'sans-serif'],
        'secondary': ['BebasNeue', 'cursive'],
      }
    },
  },
  plugins: [],
}

