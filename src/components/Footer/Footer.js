import Link from "next/link";
import { FaFacebook, FaTwitter, FaDiscord } from 'react-icons/fa';

const Footer = () => {
    return (
        <footer className="bg-gray-800">
            <div className="max-w-7xl mx-auto py-12 px-4 overflow-hidden sm:px-6 lg:px-8">
                <nav className="-mx-5 -my-2 flex flex-wrap justify-center">
                    <div className="px-5 py-2">
                        <Link href="/" className="text-base leading-6 text-gray-300 hover:text-white">
                            Home
                        </Link>
                    </div>
                    <div className="px-5 py-2">
                        <Link href="/about" className="text-base leading-6 text-gray-300 hover:text-white">
                            Partners
                        </Link>
                    </div>
                    <div className="px-5 py-2">
                        <Link href="/contact" className="text-base leading-6 text-gray-300 hover:text-white">
                            Games
                        </Link>
                    </div>
                    <div className="px-5 py-2">
                        <Link href="/contact" className="text-base leading-6 text-gray-300 hover:text-white">
                            Gallery
                        </Link>
                    </div>
                    <div className="px-5 py-2">
                        <Link href="/contact" className="text-base leading-6 text-gray-300 hover:text-white">
                            Contact
                        </Link>
                    </div>
                </nav>
                
                <div className="mt-8 flex justify-center">
                    <a href="#" className="ml-6 text-gray-400 hover:text-gray-500">
                        <FaFacebook size={24} />
                    </a>
                    <a href="#" className="ml-6 text-gray-400 hover:text-gray-500">
                        <FaTwitter size={24} />
                    </a>
                    <a href="https://discord.gg/your-server" className="ml-6 text-gray-400 hover:text-gray-500">
                        <FaDiscord size={24} />
                    </a>
                </div>
                <p className="mt-8 text-center text-base leading-6 text-gray-400">
                    © 2023 Primordials. All rights reserved.
                </p>
                <div className="flex justify-center items-center pt-6">
                    <div className="px-5">
                        <a className="text-sm leading-6 text-gray-300 hover:text-white" href="#">
                            Terms of Service
                        </a>
                    </div>
                    <div className="px-5">
                        <a className="text-sm leading-6 text-gray-300 hover:text-white" href="#">
                            Privacy Policy
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;

