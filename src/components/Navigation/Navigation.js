import { useState, useEffect } from "react";

import Link from 'next/link'
import Image from 'next/image'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDiscord, faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons'

import logo from "../../../public/images/logo.png"

const items = [
    { name: 'Home', href: '#' },
    { name: 'Partners', href: '#' },
    { name: 'Games', href: '#' },
    { name: 'Gallery', href: '#' },
    { name: 'Contact', href: '#' },
]

const Navigation = () => {

    const [scrolled, setScrolled] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            const isScrolled = window.scrollY > 50;
            if (isScrolled !== scrolled) {
                setScrolled(isScrolled);
            }
        };
        document.addEventListener('scroll', handleScroll);
        return () => {
            document.removeEventListener('scroll', handleScroll);
        };
    }, [scrolled]);

    return (
        <header className="relative w-full z-50">
            <div className="w-full bg-black text-white flex justify-end">
                <div className="grid grid-cols-3 gap-2 mr-4 mt-2">
                    <a href="#" className="hover:text-teal-400">
                        <FontAwesomeIcon icon={faTwitter} />
                    </a>
                    <a href="#" className="hover:text-teal-400">
                        <FontAwesomeIcon icon={faFacebook} />
                    </a>
                    <a href="#" className="hover:text-teal-400">
                        <FontAwesomeIcon icon={faDiscord} />
                    </a>
                </div>
            </div>
            <div className={`text-white h-20 w-full flex justify-between items-center px-10 transition duration-500 ${scrolled ? 'fixed top-0 bg-black' : 'bg-black'}`}>
                <div className="w-20 h-20">
                    <Link href="/">
                        <Image src={logo} alt="Primodorial Logo" />
                    </Link>
                </div>
                <nav>
                    {items.map((item) => (
                        <Link href={item.href} key={item.name} className="mx-4 font-secondary text-2xl hover:text-teal-400">
                            {item.name}
                        </Link>
                    ))}
                </nav>
            </div>
        </header>
    )
}

export default Navigation;