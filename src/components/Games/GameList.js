import Link from 'next/link';
import Game from './Game';

const GamesList = ({ games }) => {
    return (
        <div className="bg-gray-100 text-black">
            <div className="text-center pt-8">
                <h2 className="text-7xl font-bold uppercase font-secondary">Games We Play</h2>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-10 p-6">
                {games.map((game) => (
                    <Game key={game.id} game={game} />
                ))}
            </div>
            <h3 className="text-md text-center pb-8">Want to suggest a new game for us to organize on? <Link href="/contact" className="text-teal-700 hover:text-teal-500">Contact us</Link>!</h3>
        </div>
    );
};

export default GamesList;
