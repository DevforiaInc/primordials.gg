import Link from "next/link";

const Game = ({ game }) => {
    const { title, image, description, url } = game;

    return (
        <a href={game.url} className="text-indigo-500 group hover:bg-black font-semibold text-sm relative">
            <div className="hidden group-hover:block absolute h-full w-full bg-gray-600/50 z-10 duration-500" />
            <div className="bg-gray-800 group-hover:bg-gray-700 duration-500 shadow-lg rounded-lg overflow-hidden">
                <img className="w-full h-56 object-cover object-center" src={image} alt={title} />
                <div className="p-4">
                    <h3 className="text-white text-xl font-medium text-center">{title}</h3>
                    {/* <p className="mt-2 text-gray-600">{description}</p> */}
                </div>
            </div>
        </a>
    );
};

export default Game;
