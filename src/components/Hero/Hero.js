const Hero = () => {
    return (
        <div className="relative h-screen" >
            <div className="absolute top-0 left-0 w-full h-full overflow-hidden">
                <video autoPlay="true" muted="true" loop="true" className="absolute top-0 left-0 w-full h-full object-cover z-0">
                    <source src="/bg.mp4" type="video/mp4" />
                </video>
                <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50"></div>
            </div>
            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-center z-10">
                <h1 className="text-8xl font-bold text-gray-50 mb-4 uppercase font-primary">Primordials</h1>
                <p className="tracking-tighest text-xl text-gray-50/90 mb-8 font-primary">Experience gaming in its purest form.</p>
                <button className="transition duration-500 py-3 px-6 bg-gray-50 hover:bg-teal-300 text-sm text-black font-primary font-bold rounded-full">Join Now</button>
            </div>
        </div >
    );
};

export default Hero;
