import Image from "next/image";
import Warrior from "../../../public/images/logo_text.png"

const About = () => {
    return (
        <div className="bg-gray-900 text-white">
            <div className="max-w-7xl mx-auto">
                <div className="grid grid-cols-2 tracking-wider">
                    <div className="">
                        <Image src={Warrior} alt="About Us Image" className="mx-auto" />
                    </div>
                    <div className="my-10 text-md tracking-wider">
                        <h2 className="text-7xl font-bold mb-2 font-secondary tracking-wider">About Us</h2>
                        <p className="py-2">At Primordials, we&apos;re passionate about bringing together gamers from around the world to form a community of like-minded individuals who share a love for a wide range of games. We're a multi-game clan that actively recruits players for Ashes of Creation, Blue Protocol, Diablo IV, Diablo Immortal, Predecessor, and Throne and Liberty. We believe that by recruiting players from a variety of games, we can create a vibrant and diverse community that supports and encourages one another.</p>
                        <p className="py-2">Our mission is to provide a safe and welcoming space for gamers to come together, build meaningful connections, and engage in their favorite games. Whether you're a seasoned veteran or just starting out, we welcome players of all skill levels and backgrounds to join our community. Our focus on inclusivity and diversity allows us to create a positive and supportive environment where all players can thrive.</p>
                        <p className="py-2">Joining Primordials means joining a community of passionate gamers who share a love for gaming and a desire to connect with others who share that passion. We offer regular events, tournaments, and community challenges to help bring our members together and foster a sense of camaraderie. If you're looking for a place to call home in the gaming world, look no further than Primordials.</p>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default About;